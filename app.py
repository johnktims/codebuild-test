from chalice import Chalice

app = Chalice(app_name='bitbucket-codebuild-notifier')


@app.route('/{code_build_job_name}', methods=['POST'])
def index(code_build_job_name):
    user_as_json = app.current_request.json_body
    print(user_as_json)
